console.log("this is new-conference.js");


window.addEventListener("DOMContentLoaded", async () => {

    //! Get list of locations

    const locationUrl = "http://localhost:8000/api/locations/";

    try {

        const response = await fetch(locationUrl);

        if (!response.ok) {
            //If response is not okay, do a command
             console.log("Hello~~~~(!response.ok)");
        } else {

            const data = await response.json();
            const selectTag = document.getElementById("location"); //For id, I added in location encoder, created view_func and updated urls

            // console.log("data:", data)

            for (const [key, value] of Object.entries(data.locations[0])) {
              const option = document.createElement("option");

              option.value = value; //value is the id data.locations[0]
              option.innerHTML = key;

              selectTag.appendChild(option);
            } // end of for

        } // end of else

    } //end of try

    catch (e) {
       console.log("Hello~~~~ERROR~~~~");
    }

    //! Create Conference

    const formTag = document.getElementById("create-conference-form");

    formTag.addEventListener("submit", async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // console.log(json);

        const conferenceUrl = "http://localhost:8000/api/conferences/";

        const fetchConfig = {

            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            // console.log(newConference);
        }
    });
})
