console.log("this is new-location.js")



window.addEventListener("DOMContentLoaded", async () => {
  const stateUrl = "http://localhost:8000/api/states/";

  //! Getting list of states as option
  try {
    const response = await fetch(stateUrl);

    if (!response.ok) {
      //If response is not okay, do a command
    } else {
      const data = await response.json();
      const selectTag = document.getElementById("state");

      for (const [key, value] of Object.entries(data.states[0])) {
        //console.log(`${key}: ${value}`);
        const option = document.createElement("option");

        option.value = value;
        option.innerHTML = key;

        selectTag.appendChild(option);
      }
    } //End of else
  } catch (e) {
    //Do something when error
  }

  const formTag = document.getElementById("create-location-form");

  //! Create a location
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    // console.log(json);

    const locationUrl = "http://localhost:8000/api/locations/";

    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
      // console.log(newLocation);
    }
  });
})
