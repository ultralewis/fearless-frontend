function createCard(name, description, pictureUrl, locationName, startDate, endDate) {
  return `
  <div class="col">
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-subtitle mb-2 text-muted">${locationName}</p>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">From ${startDate} to ${endDate}</div>
    </div>
  </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {

        const errorMessage = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>I'm Sorry!</strong> Something bad happened. Please try aagin
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                `;
        const errorLocation = document.querySelector(".container");
        errorLocation.innerHTML = errorMessage;

    } else {
      const data = await response.json();
        // console.log(data)
      for (let conference of data.conferences) {

        const detailUrl = `http://localhost:8000${conference.href}`;

        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            // console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const locationName = details.conference.location.name;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();

            const html = createCard(title, description, pictureUrl, locationName, startDate, endDate);

            const column = document.querySelector(".row-cols-3");
            column.innerHTML += html;

            // const descriptionTag = document.querySelector(".card-text");
            // descriptionTag.innerHTML = description;
            // // console.log(confDescription);

            // const imageTag = document.querySelector(".card-img-top");
            // imageTag.src = details.conference.location.picture_url;
        }
      }

      // const description = data.conference
    }
  } catch (e) {

    const errorMessage = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>I'm Sorry!</strong> Something bad happened. Please try aagin
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        `;
    const errorLocation = document.querySelector(".container");
    errorLocation.innerHTML = errorMessage;
  }
});
